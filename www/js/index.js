'use strict';

document.addEventListener('deviceready', onDeviceReady, true);
document.addEventListener('resume', onResume, true);

var client = null;

function onDeviceReady() {
  console.log('Device Ready');

  if (client)
    return;

  $.post("https://zigron.goabode.com/api/auth2/login", {
    id: "demo@zigron.com",
    password: "ABc123"
  }).done(function( data ) {
    console.log("Login Done", data);
    connect();
  });
}

function onResume() {
  console.log('on resume');
}


function connect(){

  client = io.connect('https://sio.goabode.com', {
    'forceNew': true,
    'reconnection': true,
    'reconnectionAttempts': Infinity
  });

  client.on('connect', function () {
    console.log("Socket Connected!");
  });

  client.on('connect_error', function () {
    console.log("Error in connecting socket!");
  });

  client.on('disconnect', function () {
    console.log("Socket is Disconnected");
    client = null;
  });

  client.on('reconnect_attempt', function () {
    console.log("Socket is reconnecting..");
  });

  client.on('reconnect_failed', function () {
    console.log("Socket reconnection failed..");
  });

  client.on('reconnect', function () {
    console.log("Socket is reconnected!");
  });

  client.on('com.goabode.gateway.online', function (currentStatus) {
    console.log("com.goabode.gateway.online", currentStatus);
  });

  client.on('com.goabode.gateway.mode', function (newMode) {
    console.log("com.goabode.gateway.mode", newMode);
  });

  client.on('com.goabode.gateway.timeline', function (timelineObject) {
    console.log('com.goabode.gateway.timeline', timelineObject);
  });

  client.on('com.goabode.gateway.timeline.update', function (timelineObject){
    console.log("com.goabode.gateway.timeline.update", timelineObject);
  });

  client.on('com.goabode.device.update', function (deviceID) {
    console.log("com.goabode.device.update", deviceID);
  });

  client.on('com.goabode.gateway.getrecord', function(obj){
    console.log('com.goabode.gateway.getrecord', obj);
  });
};

